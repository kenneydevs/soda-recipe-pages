from django.db import models

class Recipe(models.Model):
    title = models.CharField(max_length=200)
    picture = models.URLField(max_length=200)
    description = models.TextField(max_length=500)
    created_on = models.DateTimeField(auto_now_add=True)
